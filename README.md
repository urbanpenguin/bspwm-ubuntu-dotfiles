# Bspwm Ubuntu DotFiles
DotFiles for bspwm Ubuntu 20.04

The dotfiles are for the following applications:
1. Alacritty
2. Polybar
3. Simple X Hot Key Daemon
4. Bspwm
